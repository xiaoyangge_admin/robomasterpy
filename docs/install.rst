.. _install:

Installation of robomasterpy
====================================

RoboMasterPy requires Python 3.6 and above.

::

    pip install robomasterpy


If you are using Python 3.6.x, you need to install ``dataclasses``::

    pip install dataclasses

